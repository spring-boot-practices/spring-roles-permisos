import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
// import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  public token: any;
  public decodedToken: any;
  public name: String = "";
  public userName: string = "";
  public responseUser: any;
  public responseUsers: any;
  show: Boolean = false;
  show1: Boolean = false;

  constructor(private userService: UserService,
              private router: Router ) {
    this.token = sessionStorage.getItem(
      UserService.SESSION_STORAGE_KEY
    );
    this.name = sessionStorage.getItem("userName");
    // const helper = new JwtHelperService();
    // this.decodedToken = helper.decodeToken(this.token);
    // console.log("decodeTK: ", this.decodedToken);
  }
  ngOnInit() {
  }
  public getAllUsers(){
    this.userService.getAllUsers(this.token).subscribe(
      success =>{
        this.responseUsers = success;
        this.show = false;
        this.show1 = false;
        
      },
      error => {
        console.log("Error: ", error.status);
        if (error.status == 403) {
          this.show = true;
          this.show1 = false;
        } else {
          this.show1 = true;
          this.show = false;
        }
        console.log("Error: ", error);
      }
    );
  }
  public getUser(){
    this.responseUsers = null;

    this.userService.getUser(this.userName,this.token).subscribe(
      success =>{
        this.responseUser = success;
        this.show = false;
        this.show1 = false;
      },
      error => {
        console.log("Error: ", error.status);
        if (error.status == 403) {
          this.show = true;
          this.show1 = false;
        } else {
          this.show1 = true;
          this.show = false;
        }
        console.log("Error: ", error);
      }
    );
  }

  public logOut(){
    this.userService.logOut(this.token).subscribe(
      sucess => {
        console.log('[INFO] Logout con exito!', sucess);
        this.router.navigate(['/home']);
      },
      error => {
        console.log("[ERROR] Error en logout: ", error);
      }
    );
  }
}
