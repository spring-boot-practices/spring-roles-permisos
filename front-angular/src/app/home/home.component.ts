import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { GoogleApiService } from 'ng-gapi';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // tslint:disable-next-line: ban-types
  username: String = "";
  password: String = "";
  show: Boolean = false;
  public user: String;

  constructor(private userService: UserService, private router: Router,
    private gapiService: GoogleApiService) {
    this.gapiService.onLoad().subscribe();
  }
  // public GoogleAuth;
  ngOnInit() {
  }
  public signIn() {
    this.userService.signIn().then(res => {
      this.userService.sendToRestApiMethod(res.code).then((response) => {
        this.userService.signInSuccessHandler(res.code, response.name);
        this.router.navigate(['/welcome']);
      }
      );
    });
  }
  public initClient() {
    gapi.client.init({
      apiKey: environment.apiKey,
      clientId: environment.client_id,
      scope: environment.scope,
      discoveryDocs: environment.discoveryDocs,
    }).then(function () {
      console.log('redireccion');
    });
  }
  public signInNormal() {
    this.userService.signInNormal(this.username, this.password)
      .subscribe(
        onSuccess => {
          console.log('[INFO] Logueo con exito!', onSuccess);
          this.user = onSuccess;
          this.show = false;
          this.userService.signInSuccessHandler(onSuccess.token, onSuccess.username);
          this.router.navigate(['/welcome']);

        },
        error => {
          this.show = true;
          console.log('[ERROR] Error en el Logueo!!!', error);
        }
      );
  }
}
