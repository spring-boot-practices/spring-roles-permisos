
import { Component, OnInit } from '@angular/core';
import { MessagingService } from 'src/app/services/messaging/messaging.service';

@Component({
  selector: 'messag',
  templateUrl: './messaging.component.html',
  styleUrls: ['./messaging.component.css']
})

export class MessagingComponent implements OnInit {

  message;
  private tokenNotify;
  userToSend;

  constructor(private messagingService: MessagingService) { }

  ngOnInit() {
    const userId = sessionStorage.getItem("userName");
    //Se recupera el tokenNotify 
    this.messagingService.requestPermission(userId).subscribe(
      (tokenNotify) => {
        this.tokenNotify = tokenNotify;
        console.log(tokenNotify);
        // this.messagingService.updateToken(userId, tokenNotify);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
    this.messagingService.receiveMessage()
    this.message = this.messagingService.currentMessage
  }

  public sendBroadcast() {
    this.messagingService.sendBroadcast()
      .subscribe(
        (res) => {
          console.log("Exito!! ", res);
        },
        (error) => {
          console.log("Error!! ", error);
        }
      );
  }

  public subscribeToTopic() {
    this.messagingService.subscribeToTopic('testopic', this.tokenNotify)
      .subscribe(
        (res) => {
          console.log("Exito!! ", res);
        },
        (error) => {
          console.log("Error!! ", error);
        }
      );
  }

}