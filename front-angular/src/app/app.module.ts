import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
  GoogleApiModule,
  NgGapiClientConfig,
  NG_GAPI_CONFIG} from 'ng-gapi';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserService } from './services/user.service';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';
import { MessagingComponent } from './components/messaging/messaging.component';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire';
import { MessagingService } from './services/messaging/messaging.service';
import { AsyncPipe } from '@angular/common';
import { WelcomeComponent } from './welcome/welcome.component';
import { HomeComponent } from './home/home.component';



const gapiClientConfig: NgGapiClientConfig = {
  client_id: environment.client_id,
  discoveryDocs: environment.discoveryDocs,
  scope: environment.scope
};

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    HomeComponent,
    WelcomeComponent,
    MessagingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    }),
    FormsModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [UserService, MessagingService, AsyncPipe],
  bootstrap: [AppComponent],
  exports: [HttpClientModule,
  ]
})
export class AppModule { }
