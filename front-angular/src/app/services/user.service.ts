import { Injectable, NgZone } from '@angular/core';
import { GoogleAuthService } from 'ng-gapi';
import GoogleUser = gapi.auth2.GoogleUser;
import 'rxjs/add/operator/map';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public static SESSION_STORAGE_KEY: string = 'accessToken';
  private user: GoogleUser;

  constructor(private googleAuth: GoogleAuthService, private http: HttpClient, private ngZone: NgZone) {
  }
  public getToken(): string {
    const token: string = sessionStorage.getItem(UserService.SESSION_STORAGE_KEY);
    if (!token) {
      throw new Error('no token set , authentication required');
    }
    return sessionStorage.getItem(UserService.SESSION_STORAGE_KEY);
  }
  public signIn() {
    return this.googleAuth.getAuth()
      .toPromise()
      .then((auth) => {
        return auth.grantOfflineAccess()
      }

      );
  }

  public signInSuccessHandler(res, name) {
    sessionStorage.setItem(
      "userName", name
    );
    return sessionStorage.setItem(
      UserService.SESSION_STORAGE_KEY, res
    );
  }
  public sendToRestApiMethod(token: string): any {
    return this.http.get('http://localhost:8080/api/auth/login/oauth2/client/google',
      {
        params: { code: token }
      }

    ).toPromise();
  }
  public signInNormal(username: String, password: String): any {
    // var headers = new Headers();
    // headers.append('Content-Type', 'application/json');
    return this.http.post(
      "http://localhost:8080/api/auth/user",
      JSON.stringify({ username: username, password: password })
    );
  }

  public getAllUsers(token): any {
    // var headers = new Headers();
    // headers.append('Authorization', token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.get(
      "http://localhost:8080/users/",
      httpOptions
    );
  }

  public getUser(userName,token): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.get(
      "http://localhost:8080/users/" + userName,
      httpOptions
    );
  }

  public logOut(token): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(
      "http://localhost:8080/api/auth/logout",
      JSON.stringify({ token: token }),
      httpOptions,
    );
  }

}
