export const environment = {
  baseUrl: "https://localhost:8080/",
  production: false,
  urlGoogle:"https://accounts.google.com/o/oauth2/auth?client_id=838815921051-lkegebg8vmf8t9nr046dljguno0r7hbs.apps.googleusercontent.com&response_type=code&redirect_uri=http://localhost:8080/api/auth/login/oauth2/client/google&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&state=561a30d8-e7fb-42ac-ae39-17058735e302",
  client_id: '838815921051-lkegebg8vmf8t9nr046dljguno0r7hbs.apps.googleusercontent.com',
  discoveryDocs: ['https://analyticsreporting.googleapis.com/$discovery/rest?version=v4'],
  scope: [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile'
  ].join(' '),
  apiKey: 'AIzaSyC3LiEqoFgiLB6C3J32xAndDUybBjkpj4E',
  
  firebase: {
    apiKey: "AIzaSyBkwBVqyn6KDDqr6O9Olyr_OtY237CcOzI",
    authDomain: "spring-roles-permisos.firebaseapp.com",
    databaseURL: "https://spring-roles-permisos.firebaseio.com",
    projectId: "spring-roles-permisos",
    storageBucket: "spring-roles-permisos.appspot.com",
    messagingSenderId: "650619562705",
    appId: "1:650619562705:web:70828599e9bba483",
  },
  

};
