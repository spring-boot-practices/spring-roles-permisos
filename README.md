# BACKEND AND FRONTEND LOGIN WITH SOCIAL NETWORK
Below we give a brief description of both the Backend and the frontend for authentication with social networks.
The Frontend was implemented with Angular 7, and Spring Boot with Spring Security, Hibernate, JWT, OAuth 2.0, in the Backend.

The backend allows the management of users with different profiles (User) where each one can have access to different actions (Actions), where these are grouped into modules (Modules).

### SECURING AN API REST WITH JWT AND ROLES

#### JSON WEB TOKEN (JWT)
JSON Web Token (JWT) is an open standard (RFC 7519) that defines a compact and autonomous way to securely transmit information between parts as a JSON object. This information can be verified and is reliable because it is digitally signed. JWTs can be signed using a secret (with the HMAC algorithm) or using a public / private key pair using RSA.

In the following diagram you can see the usual flow of a secure application

![Intentando Cargar](https://www.adictosaltrabajo.com/wp-content/uploads/2017/08/diagrama-JWT-300x249.png)

In the web of jwt.io you have a debugger that allows you to check and check the validity of it.

![very good](https://www.adictosaltrabajo.com/wp-content/uploads/2017/08/token-decodificado.png)



Ref: https://dev.to/keysh/spring-security-with-jwt-3j76

#### Securing with roles: @PreAuthorize
The roles allow us to have a more specific control of access to our API, being able to limit a smaller group of users certain resources of our system or protect access to sensitive or critical operations. Example:
@PreAuthorize(«hasRole(‘ROLE_ADMIN’) AND hasRole(‘ROLE_USER’)») Access to all users with admin and user roles.

#### Using OAuth 2.0 to Access Google APIs
Google APIs use the OAuth 2.0 protocol for authentication and authorization. All applications follow a basic pattern when accessing a Google API using OAuth 2.0. At a high level, you follow four steps:

![Ref](https://developers.google.com/accounts/images/webflow.png)
Ref: https://developers.google.com/identity/protocols/OAuth2



## STRUCTURE
The backend allows the management of users with different profiles (User) where each one can have access to different actions (Actions), where these are grouped into modules (Modules).

The base entities that are represented are:
 - user (username,password,profile_id)
 - profile (id, description)
 - module (id,description)
 - action (id, description,key,module_id)
 - action_grant (id,action_id,profile_id)

 ![No se puede acceder](https://i.ibb.co/CHbnv33/pict.png)

In the data.sql file there is an example with 3 users, 2 profiles, 1 action, 1 module and 1 action_grant.



----------------------------------------------------------------------------
## INSTALLATION AND RUN
For run the aplication, we need:
    - Java 1.8
    - Redis
    - NodeJs

###### Download the project in the from gitlab
     git clone git@git.grupoassa.com:gaGP/spring-roles-permisos.git  

###### Download NodeJs from   
      https://nodejs.org

Once all the environment is installed, in order to run the BackEnd, we have to create our API in the Google console.

##### HOW TO AUTHENTICATE AND AUTHORIZE AN ACCESS TO GMAIL API FROM THE APPLICATION
The Google API allows us to use their services from our own applications. For example, our applications can exchange data with Google Drive, use Gmail, calendar, Blogger, request analytical data from YouTube. In this case we will use authentication with the Google API.

![Ref](https://discourse-cdn-sjc1.com/business5/uploads/expo/optimized/2X/c/c8266a9c832f873b057ec080360bfc8c0a56bbca_2_493x500.png)

The following article presents how to authenticate and authorize access to the Gmail API from our Backend.
Ref: https://medium.com/@pablo127/google-api-authentication-with-oauth-2-on-the-example-of-gmail-a103c897fd98

Once the API is configured in the google developer console, update the following propertis in the Backend:

    client.security.google.clientId = your_client_Id
    client.security.google.clientSecret = your_client_secret
    google.client.id = your_client_Id
    google.client.secret = your_client_secret



And the following properties in the FrontEnd:

    AGREGAR LAS PROPERTIES DEL FRONT

#### RUN OF APPLICATION

##### BACKEND

To run the BackEnd, we must first run the Redis_server. After our IDE, we run the project.
We can access the administrator of the base H2, from:

    http://localhost/h2  

##### FRONTEND

To run the front, from the console, we stop inside the "front-angular" folder, execute:

    ng serve
With this command, the NodeJs serve starts and we can raise our Front application. Then from our Browser we can run it with:

    http://localhost/4200

##BROADCAST NOTIFICATIONS

With this funcionality we can subscribe and send messages to one topic. We use Firebase Cloud Messaging, Topic Messaging helps us to send a message to multiple devices that have subscribed to a particular TOPIC. We compose topic messages, then FCM handles routing and delivering the messages to devices. In this tutorial, we’re gonna look at way to subscribe TOPIC, receive Messages, then unsubscribe in an App Client.

###Configure Firebase Messaging

1- In into Firebase page:

	https://console.firebase.google.com/u/1/

2- Create a new project

![create](https://git.grupoassa.com/gaGP/spring-roles-permisos/raw/1aba37dd88cf79412a04f2f313e7788b50683edd/spring-roles-permisos/img/create%20new%20project.jpg)

3- Register the App 

![register](https://git.grupoassa.com/gaGP/spring-roles-permisos/raw/1aba37dd88cf79412a04f2f313e7788b50683edd/spring-roles-permisos/img/register%20the%20app.jpg)

4- Add SDK to App

![add_sdk](https://git.grupoassa.com/gaGP/spring-roles-permisos/raw/1aba37dd88cf79412a04f2f313e7788b50683edd/spring-roles-permisos/img/sdk.jpg)

###Configure Back-End Server-Key

1- Go to configure in Firebase console and recover the ServerKey

![get_serverkey](https://git.grupoassa.com/gaGP/spring-roles-permisos/raw/1aba37dd88cf79412a04f2f313e7788b50683edd/spring-roles-permisos/img/server%20key.jpg)

2- Set the constant FIREBASE_SERVER_KEY with your server key

![set_serverkey](https://git.grupoassa.com/gaGP/spring-roles-permisos/raw/1aba37dd88cf79412a04f2f313e7788b50683edd/spring-roles-permisos/img/add%20your%20server%20key%20to%20backend.jpg)

3- Go to Account od Service and download the file of private key

![download](https://git.grupoassa.com/gaGP/spring-roles-permisos/raw/7ba24084140b9214731ea60622420c9ae620753b/spring-roles-permisos/img/download%20file%20private%20key.jpg)

4- Put the file in the directory project

![file](https://git.grupoassa.com/gaGP/spring-roles-permisos/raw/7ba24084140b9214731ea60622420c9ae620753b/spring-roles-permisos/img/put%20the%20file%20in%20the%20directory.jpg)

5- Set the same name in the class of configuration

![configure](https://git.grupoassa.com/gaGP/spring-roles-permisos/raw/7ba24084140b9214731ea60622420c9ae620753b/spring-roles-permisos/img/configure%20class%20with%20the%20file%20s%20name.jpg)

--------------------------------------------------------------------------
##### Referencias
Ref Angular
https://www.techomoro.com/how-to-install-and-setup-angular-7-on-windows-10/

Ref Redis
https://redis.io/download
