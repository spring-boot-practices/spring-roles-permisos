insert into profile (id, description) values (1, 'ADMIN');
insert into profile (id, description) values (2, 'USER');

insert into user (username, password, profile_id) values ('lucas', '$2a$10$o3O5kMVTMUdrco1edogbwezdl5jXmBsR1//MbSVulpD3AkeMosBn6',2);
insert into user (username, password, profile_id) values ('caro', '$2a$10$o3O5kMVTMUdrco1edogbwezdl5jXmBsR1//MbSVulpD3AkeMosBn6',1);
insert into user (username, password, profile_id) values ('ale', '$2a$10$o3O5kMVTMUdrco1edogbwezdl5jXmBsR1//MbSVulpD3AkeMosBn6',2);

insert into module (id, description) values (1, 'Modulo1');

insert into plataform (id, description) values (1, 'Google');

insert into action (id, description, key, module_id) values (1,'Retorna un usuario','GET_USER',1);
insert into action (id, description, key, module_id) values (2,'Retorna todos los usuarios','GET_USERS',1);

insert into action_grant (action_id, profile_id) values (1, 2);
insert into action_grant (action_id, profile_id) values (1, 1);
insert into action_grant (action_id, profile_id) values (2, 1);




