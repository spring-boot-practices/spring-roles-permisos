package com.module.springrolespermisos.service;

import com.module.springrolespermisos.controller.data.request.ActionRequest;
import com.module.springrolespermisos.data.entity.security.Action;
import com.module.springrolespermisos.data.repository.ActionGrantRepository;
import com.module.springrolespermisos.data.repository.ActionRepository;
import com.module.springrolespermisos.error.EntityNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActionServiceImp implements ActionService {

	@Autowired
	private ActionRepository actionRepository;
	@Autowired
	private ActionGrantRepository actionGrantRepository;

	@Autowired
	private ModelMapper modelmapper;

	@Override
	public List<Action> getActions() {
		return actionRepository.findAll();
	}

	@Override
	public Action create(ActionRequest actionRequest) {
		Action action = modelmapper.map(actionRequest, Action.class);
		return actionRepository.save(action);

	}

	@Override
	public Action updateById(Long id, ActionRequest actionRequest) {

		Action action = actionRepository.getOne(id);
		action = modelmapper.map(actionRequest, Action.class);
		return actionRepository.saveAndFlush(action);

	}

	@Override
	public void deleteById(Long id) {
		actionRepository.deleteById(id);

	}

	@Override
	public List<Action> getActionsByProfileId(long id) {
		List<Action> actions = new ArrayList<>();
		actionGrantRepository.findAllByProfileId(id).forEach(actionGrant -> {
			actions.add(actionGrant.getAction());
		});
		;
		return actions;
	}

	@Override
	public Action getAction(Long id) throws NotFoundException {
		return actionRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("action.id"));
	}

	@Override
	public List<Action> getActionsByModuleId(long id) {
		return actionRepository.findAllByModuleId(id);
	}

}
