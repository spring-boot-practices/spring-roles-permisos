package com.module.springrolespermisos.service;


import com.module.springrolespermisos.data.entity.User;
import com.module.springrolespermisos.security.ExternalUser;

import java.util.List;
import java.util.Optional;

public interface UserService {
    User findByUsername(String username);

    Optional<User> findById(Long id);

    List<User> findAll();

    public User signUpCustomerFromExternal(ExternalUser eu);

}
