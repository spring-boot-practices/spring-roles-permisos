package com.module.springrolespermisos.service;

import com.module.springrolespermisos.data.entity.User;
import com.module.springrolespermisos.security.AuthToken;
import com.module.springrolespermisos.security.CustomUserDetails;
import com.module.springrolespermisos.security.ExternalUser;
import org.springframework.stereotype.Service;

@Service
public interface AuthService {
    AuthToken logInAccount(CustomUserDetails cud);

    AuthToken logInAccount(User user);

    void logOutAccount(String token);

    AuthToken loginOrSignUpExternalUser(ExternalUser eu);
}
