package com.module.springrolespermisos.service;

import com.module.springrolespermisos.data.entity.security.Action;
import com.module.springrolespermisos.data.entity.security.ActionGrant;
import com.module.springrolespermisos.data.entity.security.Profile;
import com.module.springrolespermisos.data.repository.ProfileRepository;
import com.module.springrolespermisos.error.EntityNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfileServiceImp implements ProfileService {

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private ModelMapper modelmapper;

    @Override
    public void deleteById(Long id) {
        profileRepository.deleteById(id);
    }

    @Override
    public List<Profile> ListAllProfile() {
        return profileRepository.findAll();
    }

    @Override
    public Profile getProfile(long id) {
        return profileRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("profile.id"));
    }

    @Override
    public Profile addActionToProfile(Long id, List<Long> profileRequest) {
        Profile profile = getProfile(id);

        profile.getGrants().clear();
        profile.getGrants().addAll(profileRequest.stream().map(idAction -> {
            Action a = new Action();
            a.setId(idAction);
            ActionGrant ag = new ActionGrant(a, profile);
            return ag;
        }).collect(Collectors.toSet()));

        return profileRepository.saveAndFlush(profile);
    }

    @Override
    public List<ActionGrant> getGrantAuthByProfileId(Long profileId) {
        return new ArrayList<ActionGrant>(getProfile(profileId).getGrants());
    }

}
