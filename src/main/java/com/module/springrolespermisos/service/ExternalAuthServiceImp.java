package com.module.springrolespermisos.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.module.springrolespermisos.data.entity.ExternalAuth;
import com.module.springrolespermisos.data.entity.User;
import com.module.springrolespermisos.data.entity.constants.PlatformConstants;
import com.module.springrolespermisos.data.repository.ExternalAuthRepository;
import com.module.springrolespermisos.data.repository.UserRepository;
import com.module.springrolespermisos.security.ExternalUser;
import com.module.springrolespermisos.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

@Service
public class ExternalAuthServiceImp implements ExternalAuthService{

	 @Value("${client.security.google.clientId}")
	 private String clientId;
	
	 @Value("${client.security.google.clientSecret}")
	 private String clientSecret;
	
	 @Value("${client.security.google.redirectUri}")
	 private String redirectUri;
	 
	 @Autowired
	 private UserRepository userRepository;
	 
	 @Autowired
	 private ExternalAuthRepository externalAuthRepository;
	 
	 @Autowired
	 private ObjectMapper objectMapper;
	 
	@Override
	public ExternalUser getSocialGoogleUser(String code) throws IOException {
			GoogleTokenResponse tokenResponse =
		          new GoogleAuthorizationCodeTokenRequest(
		              new NetHttpTransport(),
		              JacksonFactory.getDefaultInstance(),
		              SecurityConstants.GoogleSecurityConstants.TOKEN_SERVER_ENCODE_URL,
		              clientId,
		              clientSecret,
		              code,
		              redirectUri)  // Specify the same redirect URI that you use with your web
		                             // app. If you don't have a web version of your app, you can
		                             // specify an empty string.
		              .execute();
			
		GoogleIdToken idToken 			= tokenResponse.parseIdToken();
		GoogleIdToken.Payload payload 	= idToken.getPayload();
	    String userId					= payload.getSubject();  // Use this value as a key to identify a user.
		String email 					= payload.getEmail();
		//String name 					= (String) payload.get(SecurityConstants.GoogleSecurityConstants.NAME);
		String familyName 				= (String) payload.get(SecurityConstants.GoogleSecurityConstants.FAMILY_NAME);
		String givenName 				= (String) payload.get(SecurityConstants.GoogleSecurityConstants.GIVEN_NAME);
		
		
		return new ExternalUser(email,givenName,familyName,userId, PlatformConstants.GOOGLE);
		
	}

	@Override
	public User getExternalAuthUser(String subject) {
		return userRepository.findByExternalAuthExternalUserId(subject);
	}

	@Override
	public ExternalAuth saveExternalAuth(ExternalAuth externalAuth) {
		return externalAuthRepository.save(externalAuth);
	}

	/*@Override
	public ExternalUser getSocialFacebookUser(String code) {
		//log.debug("Calling Facebook API to validate and get profile info");
		RestTemplate restTemplate = new RestTemplate();
		String facebook = null;
		// field names which will be retrieved from facebook
		final String fields = "id,email,first_name,last_name";
		try {

			UriComponentsBuilder uriBuilderVerificationFirst = UriComponentsBuilder
					.fromUriString("https://graph.facebook.com/oauth/access_token")
					.queryParam("client_id", "403655687086899")
					.queryParam("client_secret", "828a12259f1407774f0fe47256c79696")
					.queryParam("grant_type", "client_credentials");
			
			
			JsonNode respt = objectMapper.readTree(restTemplate.getForObject(uriBuilderVerificationFirst.toUriString(), String.class));
			String input_token = code;
			String app_access_token = respt.get("access_token").asText();
			
			UriComponentsBuilder uriBuilderVerificationSecond = UriComponentsBuilder
					.fromUriString("https://graph.facebook.com/debug_token")
					.queryParam("input_token", input_token)
					.queryParam("access_token", app_access_token);

			objectMapper.readTree(restTemplate.getForObject(uriBuilderVerificationSecond.toUriString(), String.class));
			
			
			UriComponentsBuilder uriBuilder = UriComponentsBuilder
					.fromUriString("https://graph.facebook.com/v3.2/me")
					.queryParam("access_token", code)
					.queryParam("fields", fields);

			log.debug("Facebook profile uri {}", uriBuilder.toUriString());
			facebook = restTemplate.getForObject(uriBuilder.toUriString(), String.class);

			log.info("Facebook user authenticated and profile fetched successfully, details [{}]", facebook.toString());
		} catch (HttpClientErrorException e) {
			log.error("Not able to authenticate from Facebook", e);
			// throw new BadRequestException("Invalid access token");
		} catch (Exception exp) {
			log.error("User is not authorized to login into system", exp);

		}

		JsonNode jsonNode = null;
		try {
			jsonNode = objectMapper.readTree(facebook);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String id = jsonNode.get("id").asText();
		String email = jsonNode.get("email").asText();
		String firstName = jsonNode.get("first_name").asText();
		String lastName = jsonNode.get("last_name").asText();
		// return facebook;
		return new ExternalUser(email, firstName, lastName, id, PlatformConstants.FACEBOOK);
	}
	*/

}
