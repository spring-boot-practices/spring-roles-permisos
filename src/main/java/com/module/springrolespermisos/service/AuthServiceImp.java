package com.module.springrolespermisos.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.module.springrolespermisos.data.entity.User;
import com.module.springrolespermisos.data.entity.security.ActionGrant;
import com.module.springrolespermisos.security.AuthToken;
import com.module.springrolespermisos.security.CustomUserDetails;
import com.module.springrolespermisos.security.ExternalUser;
import com.module.springrolespermisos.security.SecurityConstants;
import com.module.springrolespermisos.security.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class AuthServiceImp implements AuthService {

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private ExternalAuthService externalAuthService;

    @Autowired
    private UserService userService;



    private AuthToken generateToken(CustomUserDetails cud) {

        final JWTCreator.Builder builder = JWT.create();

        //Date expiresAt = new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME);

        String token = builder
                .withClaim(com.module.springrolespermisos.security.SecurityConstants.TOKEN_NAME_FIELD, cud.getUsername())
                .withClaim(SecurityConstants.TOKEN_ID_FIELD, cud.getId())
                //.withExpiresAt(expiresAt)
                .sign(Algorithm.HMAC256(SecurityConstants.TOKEN_SECRET));

        AuthToken authToken = new AuthToken(token, cud.getUsername(), cud.getId(), cud.getProfileId(), cud.getRole());
        return authToken;
    }

    @Override
    public AuthToken logInAccount(CustomUserDetails cud) {
        AuthToken authtoken = generateToken(cud);
        tokenRepository.save(authtoken);
        return authtoken;
    }

    @Override
    public AuthToken logInAccount(User user) {

        List<ActionGrant> list_ga = profileService.getGrantAuthByProfileId(user.getProfile().getId());//cud.getAuthorities().stream().map(r -> r).collect(Collectors.toList());
        return logInAccount(new CustomUserDetails(user, list_ga));
    }


    @Override
    public void logOutAccount(String tokenString) {
        AuthToken token = new AuthToken(tokenString);
        tokenRepository.delete(token);

        DecodedJWT decodedToken = JWT
                .require(Algorithm.HMAC256(SecurityConstants.TOKEN_SECRET))
                .build()
                .verify(token.getToken());
    }

    @Override
    public AuthToken loginOrSignUpExternalUser(ExternalUser eu) {

        // si existe la cuenta del external la retorno orElse registro al usuario
        User user = Optional
                //Obtengo una cuenta previamente registrada para hacer el login
                .ofNullable(externalAuthService.getExternalAuthUser(eu.getUsuerId()))

                //si no existe la cuenta en la db, la registro.
                .orElseGet(() -> userService.signUpCustomerFromExternal(eu));

        //se logea
        AuthToken authtoken = logInAccount(user);

        return authtoken;
    }

}
