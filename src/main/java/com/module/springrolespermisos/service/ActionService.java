package com.module.springrolespermisos.service;


import com.module.springrolespermisos.controller.data.request.ActionRequest;
import com.module.springrolespermisos.data.entity.security.Action;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;

import java.util.List;

public interface ActionService {

    public Action getAction(Long id) throws NotFoundException;

    public List<Action> getActions();

    public Action create(ActionRequest action);

    public Action updateById(Long id, ActionRequest action);

    public void deleteById(Long id);

    public List<Action> getActionsByProfileId(long id);

    public List<Action> getActionsByModuleId(long id);

}
