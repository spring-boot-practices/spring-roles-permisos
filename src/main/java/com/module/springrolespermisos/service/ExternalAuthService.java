package com.module.springrolespermisos.service;

import com.module.springrolespermisos.data.entity.ExternalAuth;
import com.module.springrolespermisos.data.entity.User;
import com.module.springrolespermisos.security.ExternalUser;

import java.io.IOException;

public interface ExternalAuthService {
	
	ExternalUser getSocialGoogleUser(String code) throws IOException;
	
	//ExternalUser getSocialFacebookUser(String code);
	
	User getExternalAuthUser(String subject);
	
	ExternalAuth saveExternalAuth(ExternalAuth externalAuth);
	 
}
