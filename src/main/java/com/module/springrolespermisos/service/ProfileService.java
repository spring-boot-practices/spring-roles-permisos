package com.module.springrolespermisos.service;

import com.module.springrolespermisos.data.entity.security.ActionGrant;
import com.module.springrolespermisos.data.entity.security.Profile;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;

import java.util.List;

public interface ProfileService {


    public void deleteById(Long id);

    public List<Profile> ListAllProfile();

    public Profile getProfile(long id) throws NotFoundException;

    public Profile addActionToProfile(Long id, List<Long> profileRequest);

    public List<ActionGrant> getGrantAuthByProfileId(Long profileId);


}
