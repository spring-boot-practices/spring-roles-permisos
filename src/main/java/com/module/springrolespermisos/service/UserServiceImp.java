package com.module.springrolespermisos.service;

import com.module.springrolespermisos.data.entity.ExternalAuth;
import com.module.springrolespermisos.data.entity.Platform;
import com.module.springrolespermisos.data.entity.User;
import com.module.springrolespermisos.data.entity.constants.ProfileConstants;
import com.module.springrolespermisos.data.entity.security.Profile;
import com.module.springrolespermisos.data.repository.UserRepository;
import com.module.springrolespermisos.security.ExternalUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User signUpCustomerFromExternal(ExternalUser eu) {
        //en el caso de las cuentas logeadas por red social, el 'email' y la password son un hash generado a partir
        //del email del usuario para no quitar la posibilidad de usar ese email en una cuenta normal.

        User user = buildUserAccount(eu.getEmail(), eu.getEmail());
        ExternalAuth ea = new ExternalAuth();
        ea.setExternalUserId(eu.getUsuerId());
        ea.setUser(user);
        user.setPlatform(new Platform(){{setId(eu.getPlatform());}});

        user.setExternalAuth(ea);

        user = userRepository.saveAndFlush(user);
        return  user;
    }

    private User buildUserAccount(String userName, String password) {

        User user = new User();
        user.setProfile(new Profile() {{setId(ProfileConstants.USER);}});
        user.setUsername(userName);
        user.setPassword(passwordEncoder.encode(password));


        return user;
    }
}
