package com.module.springrolespermisos.data.repository;

import com.module.springrolespermisos.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    User findByUsername(String username);

    Optional<User> findById(Long Id);

    List<User> findAll();

    User findByExternalAuthExternalUserId(String externalUserId);


}
