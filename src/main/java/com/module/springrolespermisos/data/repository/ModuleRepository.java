package com.module.springrolespermisos.data.repository;

import com.module.springrolespermisos.data.entity.security.Module;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModuleRepository extends JpaRepository<Module, Long> {
    Module findByDescription(String description);

}
