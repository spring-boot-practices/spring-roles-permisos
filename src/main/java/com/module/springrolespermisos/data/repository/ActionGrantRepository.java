package com.module.springrolespermisos.data.repository;

import com.module.springrolespermisos.data.entity.security.ActionGrant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActionGrantRepository extends JpaRepository<ActionGrant, Long>, JpaSpecificationExecutor<ActionGrant> {
    List<ActionGrant> findAllByProfileId(Long moduleId);

    List<ActionGrant> findAllByActionId(Long actionId);

    void deleteByActionIdAndProfileId(Long actionId, Long profileId);

}
