package com.module.springrolespermisos.data.repository;

import com.module.springrolespermisos.data.entity.ExternalAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ExternalAuthRepository extends JpaRepository<ExternalAuth, Long>, JpaSpecificationExecutor<ExternalAuth>{

}
