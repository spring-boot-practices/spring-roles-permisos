package com.module.springrolespermisos.data.repository;

import com.module.springrolespermisos.data.entity.security.Action;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActionRepository extends JpaRepository<Action, Long>, JpaSpecificationExecutor<Action> {
    List<Action> findAllByModuleId(Long profileId);

    List<Action> findByKey(String key);

}
