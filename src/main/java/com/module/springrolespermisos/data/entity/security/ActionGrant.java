package com.module.springrolespermisos.data.entity.security;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ActionGrant implements Serializable {

    private static final long serialVersionUID = -7396748989684582656L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Action action;

    @NonNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Profile profile;

    public ActionGrant(@NonNull Action action, @NonNull Profile profile) {
        this.action = action;
        this.profile = profile;
    }

    public ActionGrant() {
    }

    @NonNull
    public Action getAction() {
        return action;
    }

    public void setAction(@NonNull Action action) {
        this.action = action;
    }

    @NonNull
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(@NonNull Profile profile) {
        this.profile = profile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
