package com.module.springrolespermisos.data.entity.security;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.io.Serializable;


@Entity
public class Action implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8123471095701434760L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private String description;

    private String key;

    @ManyToOne(fetch = FetchType.LAZY)
    private Module module;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
