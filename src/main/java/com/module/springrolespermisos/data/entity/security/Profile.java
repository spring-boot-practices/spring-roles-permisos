package com.module.springrolespermisos.data.entity.security;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "profile")
public class Profile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)//, mappedBy = "profile")
    @JoinColumn(name = "profile_id")
    private Set<ActionGrant> grants;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<ActionGrant> getGrants() {
        return grants;
    }

    public void setGrants(Set<ActionGrant> grants) {
        this.grants = grants;
    }


}
