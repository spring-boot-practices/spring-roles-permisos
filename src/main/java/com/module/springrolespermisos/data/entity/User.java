package com.module.springrolespermisos.data.entity;

import com.module.springrolespermisos.data.entity.security.Profile;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @ManyToOne(fetch = FetchType.LAZY)
    private Profile profile;

    @OneToOne
    private Platform platform;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private ExternalAuth externalAuth;

    public String getUsername() {
        return username.toLowerCase();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public ExternalAuth getExternalAuth() {
        return externalAuth;
    }

    public void setExternalAuth(ExternalAuth externalAuth) {
        this.externalAuth = externalAuth;
    }
}


