package com.module.springrolespermisos.data.entity.constants;

public class PlatformConstants {
	public static final Long FACEBOOK = 2l;
	public static final Long GOOGLE = 1l;
	public static final Long APP = 3l;
}
