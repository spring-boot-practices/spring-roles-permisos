package com.module.springrolespermisos.data.entity;

import javax.persistence.*;

@Entity
public class ExternalAuth {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@OneToOne(fetch = FetchType.LAZY)
	private User user;
	
	private String   externalUserId;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getExternalUserId() {
		return externalUserId;
	}

	public void setExternalUserId(String externalUserId) {
		this.externalUserId = externalUserId;
	}
}
