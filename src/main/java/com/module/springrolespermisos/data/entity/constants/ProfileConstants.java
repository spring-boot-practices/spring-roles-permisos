package com.module.springrolespermisos.data.entity.constants;

public class ProfileConstants {
    public final static Long ADMIN = 1l;
    public final static Long USER = 2l;

    public class ProfileNameConstants {
        public final static String ADMIN = "Administrador";
        public final static String USER = "Usuario";
    }
}


