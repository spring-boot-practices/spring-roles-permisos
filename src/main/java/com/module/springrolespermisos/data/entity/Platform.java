package com.module.springrolespermisos.data.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="plataform")
public class Platform implements Serializable {

	@Id
	protected Long id;

	@Column(name = "description", nullable = false)
	private String description;

	public Platform() {

	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
