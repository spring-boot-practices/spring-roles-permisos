package com.module.springrolespermisos.data.constants;

public class AnnotationConstants {
    public static final String HAS_AUTHORITY = "hasAuthority";
    public static final String MODULE = "module";
    public static final String VALUE = "value";

}
