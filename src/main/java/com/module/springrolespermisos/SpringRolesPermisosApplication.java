package com.module.springrolespermisos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.module.springrolespermisos.data.repository")
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableRedisRepositories
public class SpringRolesPermisosApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRolesPermisosApplication.class, args);
    }

}
