package com.module.springrolespermisos.error;

import org.springframework.http.HttpStatus;

import java.util.regex.Pattern;

public class ApiError {

    private HttpStatus status;
    private String message;
    private String errorKey;
    private String field;
    private String[] args;

    public ApiError(String errorKey, String field) {
        this.setErrorKey(errorKey);
        this.setField(field);
    }

    public ApiError(String errorKey, String field, String[] args) {
        this.setErrorKey(errorKey);
        this.setField(field);
        this.setArgs(args);
    }

    public ApiError(String errorKey, String[] args) {
        this.setErrorKey(errorKey);
        this.setArgs(args);
    }

    public ApiError(String errorKey) {
        this.setErrorKey(errorKey);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getErrorKey() {
        return errorKey;
    }

    public void setErrorKey(String errorKey) {
        this.errorKey = errorKey;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        //TODO arreglar esto para soportar fields con puntos
        String[] arr = field.split(Pattern.quote("."));
        this.field = arr[arr.length - 1];
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }
}

