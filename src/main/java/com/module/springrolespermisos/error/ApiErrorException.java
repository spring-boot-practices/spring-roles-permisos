package com.module.springrolespermisos.error;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class ApiErrorException extends RuntimeException {

    protected List<ApiError> errors;

    public ApiErrorException() {
    }

    public ApiErrorException(List<ApiError> errors) {
        this.errors = errors;
    }

    public ApiErrorException(ApiError error) {
        this.errors = Arrays.asList(error);
    }

    public void addError(ApiError error) {
        this.errors.add(error);
    }

    public List<ApiError> getErrors() {
        return errors;
    }

}

