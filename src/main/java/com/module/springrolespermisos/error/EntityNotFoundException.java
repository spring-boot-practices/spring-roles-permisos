package com.module.springrolespermisos.error;

import java.util.Arrays;

@SuppressWarnings("serial")
public class EntityNotFoundException extends ApiErrorException {

    public EntityNotFoundException(String field) {
        this.errors = Arrays.asList(new ApiError("errors.not_found", field));
    }

}
