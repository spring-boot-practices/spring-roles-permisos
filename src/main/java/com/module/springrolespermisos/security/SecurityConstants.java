package com.module.springrolespermisos.security;

public class SecurityConstants {

    public static final String TOKEN_SECRET = "ard$WmY+*679kDxt";

    public static final String TOKEN_NAME_FIELD = "name";

    public static final String TOKEN_ID_FIELD = "id";


    public class GoogleSecurityConstants {
        public static final String TOKEN_SERVER_ENCODE_URL = "https://www.googleapis.com/oauth2/v4/token";
        public static final String NAME = "name";
        public static final String FAMILY_NAME = "family_name";
        public static final String GIVEN_NAME = "given_name";
    }

}
