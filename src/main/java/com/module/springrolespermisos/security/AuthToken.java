package com.module.springrolespermisos.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import javax.persistence.Id;

@RedisHash("Tokens")
public class AuthToken {

    @Id
    @JsonIgnore
    private String id;

    @Indexed
    private String token;

    private Long userId;

    private String username;

    private Long profileId;

    @JsonIgnore
    private String role;


    public AuthToken(String token, String username, Long userId, Long profileId, String role) {
        super();
        this.id = token;
        this.token = token;
        this.username = username;
        this.userId = userId;
        this.profileId = profileId;
        this.role = role;
    }

    public AuthToken(String token) {
        this.id = token;
        this.token = token;
        this.username = "jaja";
    }

    public AuthToken() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
