package com.module.springrolespermisos.security;

import com.module.springrolespermisos.data.entity.User;
import com.module.springrolespermisos.data.entity.security.ActionGrant;
import com.module.springrolespermisos.service.ProfileService;
import com.module.springrolespermisos.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private ProfileService profileService;
    @Autowired
    private UserService userService;


    @Override
    public UserDetails loadUserByUsername(final String username) {

        final User user = userService.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        List<ActionGrant> auths = profileService.getGrantAuthByProfileId(user.getProfile().getId());
        return new CustomUserDetails(user, auths);

    }

}

