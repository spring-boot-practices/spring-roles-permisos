package com.module.springrolespermisos.security;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.module.springrolespermisos.data.entity.User;
import com.module.springrolespermisos.data.entity.security.ActionGrant;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public class CustomUserDetails implements UserDetails {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String username;
    private List<SimpleGrantedAuthority> authorities;
    private String role;
    private String password;
    private Long profileId;

    public CustomUserDetails(String name, String password, Long id, Long profileId) {
        this.id = id;
        this.username = name;
        this.password = password;
        this.profileId = profileId;
        this.authorities = new ArrayList<SimpleGrantedAuthority>();

    }

    public CustomUserDetails(User a, List<ActionGrant> auths) {
        this.password = a.getPassword();
        this.username = a.getUsername();
        this.profileId = a.getProfile().getId();
        setAuthsFromActionGrants(auths);
        //para el hasRole del @preAuthorize
        setRole(a.getProfile().getDescription());
        this.authorities.add(new SimpleGrantedAuthority(getRole()));
    }

    public CustomUserDetails(DecodedJWT dbToken, List<ActionGrant> auth, Long profileId, String role) {
        this.id = dbToken.getClaim(SecurityConstants.TOKEN_ID_FIELD).as(Long.class);
        this.username = dbToken.getClaim(SecurityConstants.TOKEN_NAME_FIELD).as(String.class);
        this.profileId = profileId;
        setAuthsFromActionGrants(auth);
        this.role = role;
        this.authorities.add(new SimpleGrantedAuthority(role));

    }

    private void setAuthsFromActionGrants(List<ActionGrant> auths) {
        this.authorities = auths.stream().map(auth -> new SimpleGrantedAuthority(auth.getAction().getKey())).collect(Collectors.toList());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getUsername() {

        return username;
    }

    @Override
    public boolean isAccountNonExpired() {

        return true;
    }

    @Override
    public boolean isAccountNonLocked() {

        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {

        return true;
    }

    @Override
    public boolean isEnabled() {

        return true;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = "ROLE_" + role;
    }

    public Long getProfileId() {
        return profileId;
    }
}
	

