package com.module.springrolespermisos.security;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;


public class ExternalUser implements Serializable {
	private  String 	email;
	private	 String		name;
	private  String 	lastName;
	private  String     usuerId;
	private  Long 		platform;

	public ExternalUser(String email, String name, String lastName, String usuerId, Long platform) {
		this.email = email;
		this.name = name;
		this.lastName = lastName;
		this.usuerId = usuerId;
		this.platform = platform;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsuerId() {
		return usuerId;
	}

	public void setUsuerId(String usuerId) {
		this.usuerId = usuerId;
	}

	public Long getPlatform() {
		return platform;
	}

	public void setPlatform(Long platform) {
		this.platform = platform;
	}
}
