package com.module.springrolespermisos.security.repository;

import com.module.springrolespermisos.security.AuthToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TokenRepository extends CrudRepository<AuthToken, String>, QueryByExampleExecutor<AuthToken> {
    List<AuthToken> findAllByProfileId(Long profileId);

}

