package com.module.springrolespermisos.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.module.springrolespermisos.data.entity.security.ActionGrant;
import com.module.springrolespermisos.security.AuthToken;
import com.module.springrolespermisos.security.CustomUserDetails;
import com.module.springrolespermisos.security.SecurityConstants;
import com.module.springrolespermisos.security.repository.TokenRepository;
import com.module.springrolespermisos.service.ProfileService;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private com.module.springrolespermisos.security.repository.TokenRepository tokenRepository;

    private ProfileService profileService;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager, TokenRepository tokenRepository, ProfileService profileService) {
        super(authenticationManager);
        this.tokenRepository = tokenRepository;
        this.profileService = profileService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HttpHeaders.AUTHORIZATION);
        if (header == null) {
            chain.doFilter(req, res);
            return;
        }
        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (token != null) {

            AuthToken dbToken = tokenRepository.findById(token).orElse(null);

            if (dbToken != null) {
                DecodedJWT decodedToken = null;
                try {
                    decodedToken = JWT
                            .require(Algorithm.HMAC256(SecurityConstants.TOKEN_SECRET))
                            .build()
                            .verify(token);

                } catch (TokenExpiredException e) {
                    tokenRepository.delete(dbToken);
                    return null;
                }
                String user = decodedToken.getClaim(SecurityConstants.TOKEN_NAME_FIELD).as(String.class);
                if (user != null) {
                    List<ActionGrant> auth = profileService.getGrantAuthByProfileId(dbToken.getProfileId());
                    CustomUserDetails cud = new CustomUserDetails(decodedToken, auth, dbToken.getProfileId(), dbToken.getRole());
                    return new UsernamePasswordAuthenticationToken(cud, null, cud.getAuthorities());
                }

                return null;
            }
            return null;
        }
        return null;
    }

}
