package com.module.springrolespermisos.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpMatcher {

    public static String getAnnotationValue(String tag, String text, boolean withquotation) {
        String value = "";
        String pattern = withquotation ? Pattern.quote(tag + "('") + "(.*?)" + Pattern.quote("')") :
                Pattern.quote(tag + "(") + "(.*?)" + Pattern.quote(")");

        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        while (m.find()) {
            value = m.group(1);
        }
        return value;
    }

}
