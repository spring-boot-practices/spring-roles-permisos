package com.module.springrolespermisos.utils;

import com.module.springrolespermisos.data.constants.AnnotationConstants;
import com.module.springrolespermisos.data.entity.constants.ProfileConstants;
import com.module.springrolespermisos.data.entity.security.Action;
import com.module.springrolespermisos.data.entity.security.ActionGrant;
import com.module.springrolespermisos.data.entity.security.Module;
import com.module.springrolespermisos.data.entity.security.Profile;
import com.module.springrolespermisos.data.repository.ActionGrantRepository;
import com.module.springrolespermisos.data.repository.ActionRepository;
import com.module.springrolespermisos.data.repository.ModuleRepository;
import com.module.springrolespermisos.data.repository.ProfileRepository;
import com.module.springrolespermisos.security.ModuleAction;
import com.module.springrolespermisos.utils.data.ActionDetail;
import io.swagger.annotations.ApiOperation;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ActionGrantsScanLoader implements ApplicationRunner {

    private ActionRepository actionRepository;
    private ActionGrantRepository actionGrantRepository;
    private ModuleRepository moduleRepository;
    private ProfileRepository profileRepository;
    private ListableBeanFactory bf;

    @Autowired
    public ActionGrantsScanLoader(ActionRepository actionRepository, ActionGrantRepository actionGrantRepository,
                                  ModuleRepository moduleRepository, ProfileRepository profileRepository, ListableBeanFactory bf) {
        this.actionRepository = actionRepository;
        this.actionGrantRepository = actionGrantRepository;
        this.moduleRepository = moduleRepository;
        this.profileRepository = profileRepository;
        this.bf = bf;
    }


    private HashMap<String, List<ActionDetail>> getGrantsAndActionsFromRestControllers() {
        HashMap<String, List<ActionDetail>> listAuthorize = new HashMap<String, List<ActionDetail>>();

        bf.getBeansWithAnnotation(ModuleAction.class).forEach((key, bean) -> {
            ModuleAction moduleAnn;
            String module = "";
            List<ActionDetail> actions = new ArrayList<ActionDetail>();
            Method[] methods = null;

            if (AopUtils.isAopProxy(bean)) {
                methods = AopUtils.getTargetClass(bean).getDeclaredMethods();
                moduleAnn = AopUtils.getTargetClass(bean).getAnnotation(ModuleAction.class);
            } else {
                methods = bean.getClass().getDeclaredMethods();
                moduleAnn = bean.getClass().getDeclaredAnnotation(ModuleAction.class);
            }

            if (moduleAnn != null) {
                module = moduleAnn.value()[0];
            }


            for (Method method : methods) {
                ActionDetail ad = new ActionDetail();

                PreAuthorize ann = method.getAnnotation(PreAuthorize.class);
                if (ann != null) {
                    String text = ann.value();
                    String action = RegExpMatcher.getAnnotationValue(AnnotationConstants.HAS_AUTHORITY, text, true);
                    if (StringUtils.isEmpty(action)) {
                        ann = null;
                    } else {
                        ad.setKey(action);
                    }
                }

                ApiOperation apiOpAnn = method.getAnnotation(ApiOperation.class);

                //Solo anexa si posee la descripcion del apioperation y posee el HasAuthority
                if (ann != null && apiOpAnn != null && !StringUtils.isEmpty(ad.getKey())) {
                    String text = apiOpAnn.value();
                    ad.setDescription(text);
                    actions.add(ad);
                }
            }
            listAuthorize.put(module, actions);

        });
        return listAuthorize;
    }

    private void processActionsAnModules(HashMap<String, List<ActionDetail>> listActions) {
        Set<String> modulesNamesEndpoint = new HashSet<String>();
        //Lo utilizare posteriormente si necesito crear acciones.
        Profile pro = profileRepository.findById(ProfileConstants.ADMIN).get();

        //Tratamos las acciones por modulo
        listActions.forEach((key, listAction) -> {
            //Obtengo el modulo
            final Module mod = moduleRepository.findByDescription(key);
            //Recorremos las acciones que posee.

            //Voy almacenando los modulos para luego poder validar si tengo que eliminarlos de la base.
            modulesNamesEndpoint.add(key);

            //SI EXISTE EL MODULO EN LA BASE
            if (mod != null) {
                //Recorremos los action de la base y luego validemos si los endpoint existen sino los creamos.
                Set<String> actionNamesDB =
                        mod.getActions().stream()
                                .map(Action::getKey)
                                .collect(Collectors.toSet());

                List<ActionDetail> actionsToSave = listAction.stream()
                        .filter(e -> !actionNamesDB.contains(e.getKey()))
                        .collect(Collectors.toList());
                //Filtro los que no estan entonces son los que deberiamos crear en la base
                actionsToSave.stream().forEach(action ->
                {
                    Action newAction = new Action();
                    newAction.setDescription(action.getDescription());
                    newAction.setKey(action.getKey());
                    newAction.setModule(mod);
                    newAction = actionRepository.save(newAction);


                    //Grabamos los actionGrants para asociarlos al Administrador
                    ActionGrant agToSave = new ActionGrant();
                    agToSave.setAction(newAction);
                    agToSave.setProfile(pro);
                    actionGrantRepository.save(agToSave);
                });


                //Si encontramos un endpoint que fue eliminado, entonces eliminamos de la base.

                Set<String> actionNamesFromEndpoints =
                        listAction.stream()
                                .map(ActionDetail::getKey)
                                .collect(Collectors.toSet());

                List<Action> actionsToDelete = mod.getActions().stream()
                        .filter(e -> !actionNamesFromEndpoints.contains(e.getKey()))
                        .collect(Collectors.toList());


                deleteActions(actionsToDelete);

            }
            //SI NO EXISTE EL MODULO SE CREA JUNTO CON LOS ACTIONS
            else {
                //Eliminamos los actions que esten huerfanos si es  que existen, luego los anexamos a una lista para poder crearlos con el
                //modulo.
                Set<Action> newActions = new HashSet<Action>();

                //Borramos acciones huerfanas
                listAction.stream().forEach(action -> {
                    List<Action> actionToDelete = actionRepository.findByKey(action.getKey());

                    deleteActions(actionToDelete.stream().collect(Collectors.toList()));

                    Action actionToSave = new Action();
                    actionToSave.setDescription(action.getDescription());
                    actionToSave.setKey(action.getKey());
                    newActions.add(actionToSave);
                });
                Module modul = new Module();
                modul.setDescription(key);
                modul.setActions(newActions);


                Module modSaved = moduleRepository.save(modul);
                modSaved.getActions().stream().forEach(action -> {
                            //Grabamos los actionGrants para asociarlos al Administrador
                            ActionGrant agToSave = new ActionGrant();
                            agToSave.setAction(action);
                            agToSave.setProfile(pro);
                            actionGrantRepository.save(agToSave);
                        }

                );


            }

        });

    }


    private void deleteActions(List<Action> actionsToDelete) {
        actionsToDelete.stream().forEach(action ->
        {
            //Eliminamos desde la tabla de actiongrants
            List<ActionGrant> ag = actionGrantRepository.findAllByActionId(action.getId());
            actionGrantRepository.deleteAll(ag);
            //Eliminamos desde la tabla Action
            actionRepository.deleteById(action.getId());
        });
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        HashMap<String, List<ActionDetail>> listModuleAndAction = getGrantsAndActionsFromRestControllers();
        processActionsAnModules(listModuleAndAction);
    }


}
