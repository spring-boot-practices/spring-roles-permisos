package com.module.springrolespermisos.controller;

import com.module.springrolespermisos.controller.data.response.UserResponse;
import com.module.springrolespermisos.security.ModuleAction;
import com.module.springrolespermisos.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@ModuleAction("Modulo1")
@RequestMapping("/users")
public class TestController {

    @Autowired
    UserService userService;
    @Autowired
    private ModelMapper modelmapper;

    @PreAuthorize("hasAuthority('GET_USERS')")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(authorizations = @Authorization(value = "apiKey"), value = "Retorna todos los usuarios")
    public List<UserResponse> getUsers() {
        return userService.findAll().stream().map(user -> modelmapper.map(user, UserResponse.class)).collect(Collectors.toList());

    }

    @PreAuthorize("hasAuthority('GET_USER')")
    @RequestMapping(value = "/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(authorizations = @Authorization(value = "apiKey"), value = "Retorna un usuario")
    public UserResponse getUser(@PathVariable("username") String username) throws NotFoundException {

        return modelmapper.map(userService.findByUsername(username), UserResponse.class);

    }

}
