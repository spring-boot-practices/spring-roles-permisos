package com.module.springrolespermisos.controller.data.response;

import com.module.springrolespermisos.data.entity.User;

public class UserResponse {

    private String username;

    private ProfileResponse role;

    public UserResponse() {
    }

    public UserResponse(User user) {
        this.setUsername(user.getUsername());
        this.setRole(new ProfileResponse(user.getProfile()));

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ProfileResponse getRole() {
        return role;
    }

    public void setRole(ProfileResponse role) {
        this.role = role;
    }


}
