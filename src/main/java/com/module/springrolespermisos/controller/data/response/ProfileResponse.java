package com.module.springrolespermisos.controller.data.response;

import com.module.springrolespermisos.data.entity.security.Profile;

public class ProfileResponse {

    private Long id;

    private String description;

    public ProfileResponse(Profile p) {
        setId(p.getId());
        setDescription(p.getDescription());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

