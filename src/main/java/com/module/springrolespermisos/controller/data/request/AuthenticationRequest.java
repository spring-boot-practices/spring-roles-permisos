package com.module.springrolespermisos.controller.data.request;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class AuthenticationRequest implements Serializable {
    private static final long serialVersionUID = -8445943548965154778L;
    @NotNull(message = "errors.generic.not_null")
    private String username;
    @NotNull(message = "errors.generic.not_null")
    private String password;

    public AuthenticationRequest() {

    }

    public AuthenticationRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}