package com.module.springrolespermisos.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.module.springrolespermisos.security.AuthToken;
import com.module.springrolespermisos.security.ExternalUser;
import com.module.springrolespermisos.service.AuthService;
import com.module.springrolespermisos.service.ExternalAuthService;
import com.module.springrolespermisos.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Optional;
@RestController
@RequestMapping("/api/auth")
@Validated
@Api(value="Endpoints para las operaciones de autenticacion")
@Transactional
public class AuthController {
	
	@Autowired
	private UserService userService;

	@Autowired
	private AuthService authService;

	@Autowired
	private ExternalAuthService externalAuthService;

	@RequestMapping(value = {"/login/oauth2/client/google"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Crea un token de autenticacion para un operador (app de delivery)")
	public String loginOrRegisterGoogleUser(@Validated @RequestParam("code") String code) throws IOException {
		
		ExternalUser eu = externalAuthService.getSocialGoogleUser(code);
		ObjectMapper mapper = new ObjectMapper();

		String json = mapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(eu);

		authService.loginOrSignUpExternalUser(eu);
		return  json;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	@ApiOperation(value = "Endpoint de logout")
	public ResponseEntity<?> logout(@RequestHeader("Authorization") String token, HttpServletRequest request) throws AuthenticationException {

		authService.logOutAccount(token);
		return ResponseEntity.ok().build();
	}

}
