package com.module.springrolespermisos.config;


import com.module.springrolespermisos.security.CustomUserDetailsService;
import com.module.springrolespermisos.security.jwt.JWTAuthenticationFilter;
import com.module.springrolespermisos.security.jwt.JWTAuthorizationFilter;
import com.module.springrolespermisos.security.repository.TokenRepository;
import com.module.springrolespermisos.service.AuthService;
import com.module.springrolespermisos.service.ProfileService;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {
    
	private static final String CUSTOMER_LOGIN_URL = "/api/auth/user";
	
    @Autowired
	private ListableBeanFactory bf;
	
    
	private static final String[] AUTH_WHITELIST = {
            // -- swagger ui
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/loggers/**",
			"/users/**",
			"/api/auth/**",
			"/send",
			"/subscribe/{topic}",
			"/logout"
    };

    @Autowired
    private CustomUserDetailsService CustomUserDetailsService;


	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private AuthService authService;

	@Autowired
	private ProfileService profileService;


	@Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(CustomUserDetailsService).passwordEncoder(encoder());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
    	

    	http.cors().and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()

		.csrf().disable()
		.formLogin().disable()
		.httpBasic().disable()
		.logout().disable()
	
		.authorizeRequests()
			.antMatchers(AUTH_WHITELIST).permitAll()

				.antMatchers("/h2/**").permitAll()
		    .anyRequest().authenticated()
		.and()
		.addFilter(new JWTAuthenticationFilter(authenticationManager(), authService) {{setFilterProcessesUrl(CUSTOMER_LOGIN_URL);}})
		.addFilter(new JWTAuthorizationFilter(authenticationManager(), tokenRepository, profileService));
		http.csrf().disable();
		http.headers().frameOptions().disable();
    }


    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    
	@Bean
    CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList("*"));
		configuration.setAllowedMethods(Arrays.asList("GET", "POST", "OPTIONS", "DELETE", "PUT", "PATCH"));
		configuration.setAllowedHeaders(Arrays.asList("X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization", "Set-Cookie", "Cookies","nonce"));
		configuration.setExposedHeaders(Arrays.asList("Authorization", "Set-Cookie", "Cookies", "nonce"));
		configuration.setAllowCredentials(true);
		configuration.setMaxAge(31536000L);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
}